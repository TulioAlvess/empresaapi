﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmpresaApi.Dados;
using EmpresaApi.Models;

namespace EmpresaApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Empregados_SetorController : ControllerBase
    {
        private readonly EmpresaContext _context;

        public Empregados_SetorController(EmpresaContext context)
        {
            _context = context;
        }

        // GET: api/Empregados_Setor
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Empregado_Setor>>> GetEmpregados_setor()
        {
            return await _context.Empregados_setor.ToListAsync();
        }

        // GET: api/Empregados_Setor/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Empregado_Setor>> GetEmpregado_Setor(int id)
        {
            var empregado_Setor = await _context.Empregados_setor.FindAsync(id);

            if (empregado_Setor == null)
            {
                return NotFound();
            }

            return empregado_Setor;
        }

        // PUT: api/Empregados_Setor/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmpregado_Setor(int id, Empregado_Setor empregado_Setor)
        {
            if (id != empregado_Setor.empregadoId)
            {
                return BadRequest();
            }

            _context.Entry(empregado_Setor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Empregado_SetorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Empregados_Setor
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Empregado_Setor>> PostEmpregado_Setor(Empregado_Setor empregado_Setor)
        {
            _context.Empregados_setor.Add(empregado_Setor);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (Empregado_SetorExists(empregado_Setor.empregadoId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetEmpregado_Setor", new { id = empregado_Setor.empregadoId }, empregado_Setor);
        }

        // DELETE: api/Empregados_Setor/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Empregado_Setor>> DeleteEmpregado_Setor(int id)
        {
            var empregado_Setor = await _context.Empregados_setor.FindAsync(id);
            if (empregado_Setor == null)
            {
                return NotFound();
            }

            _context.Empregados_setor.Remove(empregado_Setor);
            await _context.SaveChangesAsync();

            return empregado_Setor;
        }

        private bool Empregado_SetorExists(int id)
        {
            return _context.Empregados_setor.Any(e => e.empregadoId == id);
        }
    }
}
