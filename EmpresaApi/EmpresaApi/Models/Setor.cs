﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EmpresaApi.Dados;
namespace EmpresaApi.Models
{
    public class Setor
    {
        public int Id { get; set; }
        public string area { get; set; }
        
        public List<Empregado_Setor> Empregados_Setor { get; set; }
        public  Gerente Gerente { get; set; }

    }
}
