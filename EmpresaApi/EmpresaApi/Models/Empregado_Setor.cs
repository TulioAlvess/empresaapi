﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EmpresaApi.Dados;
namespace EmpresaApi.Models
{
    public class Empregado_Setor
    {
        public int empregadoId { get; set; }
        public int setorId { get; set; }

        public  Empregado Empregado { get; set; }
        public  Setor Setor { get; set; }
    }
}
