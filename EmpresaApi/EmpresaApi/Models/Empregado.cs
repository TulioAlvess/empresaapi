﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EmpresaApi.Dados;
namespace EmpresaApi.Models
{
    public class Empregado
    {
        public int Id { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public bool ativo { get; set; }
        public List<Empregado_Setor> Empregados_Setor  { get; set; }
    }
}
