﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmpresaApi.Models;
using Microsoft.EntityFrameworkCore;

namespace EmpresaApi.Dados
{
    public class EmpresaContext : DbContext
    {
           public EmpresaContext(DbContextOptions<EmpresaContext>options): base(options) 
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Empregado_Setor>()
                .HasKey(ac => new { ac.empregadoId, ac.setorId });
        }
        public DbSet<Empregado> Empregado { get; set; }
        public DbSet<Setor> Setor { get; set; }
        public DbSet<Gerente> Gerente { get; set; }
        public DbSet<Empregado_Setor> Empregados_setor { get; set; }
    }
}
